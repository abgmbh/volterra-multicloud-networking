locals {
  aws_name   = format("%s-aws", var.prefix)
  azure_name = format("%s-azure", var.prefix)
  namespace  = var.volterra_namespace_exists ? join("", data.volterra_namespace.this.*.name) : join("", volterra_namespace.this.*.name)
}
