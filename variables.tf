variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {}
variable "azure_client_id" {}
variable "azure_client_secret" {}
variable "azure_subscription_id" {}
variable "azure_tenant_id" {}
variable "azure_region" {}
variable "api_url" {}

variable "prefix" {
  type        = string
  description = "MCN Name. Also used as a prefix in names of related resources."
  default     = "cz-volterra"
}

variable "aws_client_instance_type" {
  type        = string
  description = "AWS Client VM instance type"
  default     = "t2.micro"
}

variable "aws_az" {
  type        = string
  description = "AWS Availability Zone in which the site will be created"
  default     = "us-east-1a"
}

variable "aws_vpc_cidr" {
  type        = string
  description = "AWS VPC CIDR, that will be used to create the vpc while creating the site"
  default     = "192.168.0.0/22"
}

variable "site_disk_size" {
  type        = number
  description = "Disk size in GiB"
  default     = 80
}

variable "aws_instance_type" {
  type        = string
  description = "AWS instance type used for the Volterra site"
  default     = "t3.2xlarge"
}

variable "aws_subnet_ce_cidr" {
  type        = map(string)
  description = "Map to hold different CE cidr with key as name of subnet"
  default = {
    "outside"  = "192.168.0.0/24"
    "inside"   = "192.168.1.0/24"
    "workload" = "192.168.2.0/24"
  }
}

variable "client_ami_id" {
  type        = string
  description = "Client VM ami-id, this will be different per AWS region. [List of ubuntu ami-id's, could be found here](https://cloud-images.ubuntu.com/locator/ec2/)"
  default     = "ami-04b9e92b5572fa0d1"
}

variable "client_disk_size" {
  type        = number
  description = "Client VM disk size in GiB"
  default     = 30
}

variable "ssh_public_key" {
  type        = string
  description = "SSH Public Key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCooBbRiFm3UDEty/ch1ZZHKwaTBNBwVJZIDwBuvVjPWnS3RkYMFCGFazIkJJ35QznN2o6nJZb0d8KkakkS7owgJl3ghgEmwfROOFo+EEjT5Y5gbetgs9NGHKVFRnESO3S7PF+Uk/tux4/7ReTyVKBUVfCHpr9I/uwuLHWLO87a88JZSm3qbnhHOdwU4Z8+wxNqk+4qHnvRpCq5HIJxwJoC6IbKvrjDN0YXI7yCG3aIbanSFImbDQE5xFFH+FBRBZlp8ovwajiekZ44BAo+/UJ6/dTjaYTd67a+Sq0i/jngqQ+hVqLVB8S2HQzo9l9JX08KWn92euuBDz+StUO7hIHF Chris@MBP.local"
}

variable "client_ssh_port" {
  type        = number
  description = "TCP lb listen port for ssh access to the clients"
  default     = 2220
}

variable "volterra_namespace_exists" {
  type        = string
  description = "Flag to create or use existing volterra namespace"
  default     = false
}

variable "volterra_namespace" {
  type        = string
  description = "Volterra app namespace where the object will be created. This cannot be system or shared ns."
  default     = "cz"
}

variable "azure_peer_vnet_cidr" {
  type        = string
  description = "Spoke peering vnet"
  default     = "10.1.0.0/22"
}

variable "azure_peer_subnet_cidr" {
  type        = map(string)
  description = "Map to hold different CE cidr with key as name of subnet"
  default = {
    "s0" = "10.1.0.0/24"
    "s1" = "10.1.1.0/24"
    "s2" = "10.1.2.0/24"
  }
}

variable "azure_peer_resource_group" {
  type        = string
  description = "Spoke Peer infra objects are created here"
  default     = "cz-volterra-peer"
}

variable "azure_site_resource_group" {
  type        = string
  description = "Site specific objects are created here"
  default     = "cz-volterra-site"
}

variable "azure_az" {
  type        = string
  description = "Azure Availability Zone in which the site will be created"
  default     = 1
}

variable "azure_machine_type" {
  type        = string
  description = "Azure Vnet Site machine type"
  default     = "Standard_D3_v2"
}
