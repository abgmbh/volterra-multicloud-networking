resource "volterra_origin_pool" "aws_client_ssh" {
  name      = format("%s-client-ssh", local.aws_name)
  namespace = "default"
  origin_servers {
    private_ip {
      ip              = aws_instance.this.private_ip
      inside_network  = true
      outside_network = false
      site_locator {
        site {
          name      = volterra_aws_vpc_site.this.name
          namespace = "system"
        }
      }
    }
  }
  no_tls                 = true
  port                   = 22
  same_as_endpoint_port  = true
  loadbalancer_algorithm = "LB_OVERRIDE"
  endpoint_selection     = "LOCAL_PREFERRED"
}

resource "volterra_tcp_loadbalancer" "aws_client_ssh" {
  name                            = format("%s-client-ssh", local.aws_name)
  namespace                       = "default"
  listen_port                     = var.client_ssh_port
  with_sni                        = false
  dns_volterra_managed            = false
  advertise_on_public_default_vip = true
  retract_cluster                 = false
  origin_pools_weights {
    pool {
      name = volterra_origin_pool.aws_client_ssh.name
    }
    weight = 1
  }
  do_not_advertise               = false
  hash_policy_choice_round_robin = true
}

resource "volterra_origin_pool" "azure_client_ssh" {
  depends_on = [volterra_azure_vnet_site.this]
  name       = format("%s-client-ssh", local.azure_name)
  namespace  = "default"
  origin_servers {
    private_ip {
      ip              = "10.1.0.4"
      inside_network  = true
      outside_network = false
      site_locator {
        site {
          name      = volterra_azure_vnet_site.this.name
          namespace = "system"
        }
      }
    }
  }
  no_tls                 = true
  port                   = 22
  same_as_endpoint_port  = true
  loadbalancer_algorithm = "LB_OVERRIDE"
  endpoint_selection     = "LOCAL_PREFERRED"
}

resource "volterra_tcp_loadbalancer" "azure_client_ssh" {
  name                            = format("%s-client-ssh", local.azure_name)
  namespace                       = "default"
  listen_port                     = 2221
  with_sni                        = false
  dns_volterra_managed            = false
  advertise_on_public_default_vip = true
  retract_cluster                 = false
  origin_pools_weights {
    pool {
      name = volterra_origin_pool.azure_client_ssh.name
    }
    weight = 1
  }
  do_not_advertise               = false
  hash_policy_choice_round_robin = true
}
