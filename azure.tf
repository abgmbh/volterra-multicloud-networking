resource "azurerm_resource_group" "peer" {
  name     = var.azure_peer_resource_group
  location = var.azure_region
}

module "vnet" {
  depends_on          = [azurerm_resource_group.peer]
  source              = "Azure/vnet/azurerm"
  resource_group_name = azurerm_resource_group.peer.name
  address_space       = [var.azure_peer_vnet_cidr]
  subnet_prefixes     = values(var.azure_peer_subnet_cidr)
  subnet_names        = keys(var.azure_peer_subnet_cidr)

  tags = {
    "Name" = var.azure_peer_resource_group
  }
}

resource "azurerm_network_interface" "ext-1-nic" {
  name                = "ext-1-nic"
  location            = azurerm_resource_group.peer.location
  resource_group_name = azurerm_resource_group.peer.name

  ip_configuration {
    name                          = "primary"
    subnet_id                     = module.vnet.vnet_subnets[0]
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.1.0.4"
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.ext-1-pip.id
  }
}

resource "azurerm_public_ip" "ext-1-pip" {
  name                = "ext-1-pip"
  location            = azurerm_resource_group.peer.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.peer.name
  allocation_method   = "Static"
}

resource "azurerm_network_security_group" "ext-nsg" {
  name                = "ext-nsg"
  location            = azurerm_resource_group.peer.location
  resource_group_name = azurerm_resource_group.peer.name

  security_rule {
    name                       = "allow_all"
    description                = "Allow all access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_interface_security_group_association" "ext-nsg-ass" {
  network_interface_id      = azurerm_network_interface.ext-1-nic.id
  network_security_group_id = azurerm_network_security_group.ext-nsg.id
}

resource "azurerm_linux_virtual_machine" "ubuntu-vm" {
  name                            = "ubuntu-vm"
  location                        = azurerm_resource_group.peer.location
  resource_group_name             = azurerm_resource_group.peer.name
  network_interface_ids           = [azurerm_network_interface.ext-1-nic.id]
  size                            = "Standard_F4s_v2"
  admin_username                  = "ubuntu"
  disable_password_authentication = true
  computer_name                   = "ubuntu-vm"

  os_disk {
    name                 = "ubuntu-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = "ubuntu"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}

resource "azurerm_route_table" "udr-some-IP" {
  name                = "udr-some-IP"
  location            = azurerm_resource_group.peer.location
  resource_group_name = azurerm_resource_group.peer.name

  route {
    name                   = "to-some-IP"
    address_prefix         = "192.168.2.0/24"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.1.4"
  }
}

resource "azurerm_subnet_route_table_association" "udr-some-IP" {
  subnet_id      = module.vnet.vnet_subnets[0]
  route_table_id = azurerm_route_table.udr-some-IP.id
}







