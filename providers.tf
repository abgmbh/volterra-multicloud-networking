terraform {
  required_version = ">= 0.12.9, != 0.13.0"

  required_providers {
    volterra = {
      source  = "volterraedge/volterra"
      version = "0.7.1"
    }
    aws        = "~> 3.3.0"
    null       = ">= 3.0"
    kubernetes = "~> 1.9"
    local      = ">= 2.0"
  }
}

provider "volterra" {
  api_cert = "files/volterra.cer"
  api_key  = "files/volterra.key"
  url      = var.api_url
}

provider "azurerm" {
  features {}
  client_id       = var.azure_client_id
  client_secret   = var.azure_client_secret
  subscription_id = var.azure_subscription_id
  tenant_id       = var.azure_tenant_id
}

provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

